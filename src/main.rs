use std::os::unix::net::UnixDatagram;

fn main() -> std::io::Result<()> {
    loop {
        let socket = UnixDatagram::bind("127.0.0.1:7777")?;

        let mut buffer = [0; 1024];

        let (bytes_count, sender_address) = socket.recv_from(&mut buffer)?;

        let buffer = &mut buffer[..bytes_count];

        let char_buffer = buffer.iter().map(|x| *x as char).collect::<Vec<char>>();

        println!("Received: {:x?}\nFrom: {:?}\nText: {:?}\n", &buffer, sender_address, char_buffer);

        buffer.reverse();

        //socket.send_to(buffer, &sender_address)?;
    }
}
