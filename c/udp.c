#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#define BUF_LEN 512
#define PORT 8083


void die(char *s) {
	perror(s);

	exit(1);
}

int main (int argc, char** argv) {
	puts("Starting UDP Server!");

	// Create UDP socket
	int sock = -1;

	if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		die("Error creating socket!");
	}

	// Prepare sockaddr_in struct
	struct sockaddr_in si_me;

	memset((char *) &si_me, 0, sizeof(si_me));

	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(PORT);
	si_me.sin_addr.s_addr = htonl(INADDR_ANY);

	// Bind socket to port
	if (bind(sock, (struct sockaddr*) &si_me, sizeof(si_me)) == -1) {
		die("Error while binding socket to port!");
	}

	char buffer[BUF_LEN];
	
	struct sockaddr_in si_other;

	unsigned int slen = sizeof(si_other);

	// Loop for listening for data
	while (1) {
		puts("Waiting for data...");

		fflush(stdout);

		memset(buffer, '\0', BUF_LEN);

		// Try to receive data
		int recv_len = -1;

		if ((recv_len = recvfrom(sock, buffer, BUF_LEN, 0, (struct sockaddr *) &si_other, &slen)) == -1) {
			close(sock);

			die("Error while roceiving data");
		}

		buffer[strcspn(buffer, "\n")] = 0;

		printf("Received packet from [%s:%d]: %s\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port), buffer);

		if (!strcmp(buffer, "shutdown")) {
			puts("Received shutdown command! Switching server off!");

			break;
		}

		// Send answer
		if (sendto(sock, buffer, recv_len, 0, (struct sockaddr*) &si_other, slen) == -1) {
			close(sock);

			die("Error sending answer");
		}
	}

	close(sock);

	return 0;
}
